import React from 'react';
import Tilt from 'react-tilt';
import robo from './robo.png';
import './Logo.css';


const Logo = () => {
  return (
    <div className='center pv3' >
      <Tilt className="Tilt br2 shadow-2" options={{ max : 43 }} style={{ height: 150, width: 150 }} >
        <div className="Tilt-inner pa3">
          <img src={robo} alt="logo" style={{paddingTop: '5px'}} />
        </div>
      </Tilt>
    </div>
  );
};

export default Logo;